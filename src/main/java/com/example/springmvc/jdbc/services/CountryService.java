package com.example.springmvc.jdbc.services;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.springmvc.jdbc.exceptions.ResourceAlreadyExistsException;
import com.example.springmvc.jdbc.model.Country;

@Service
public class CountryService {

	private JdbcTemplate jdbcTemplate;

	public CountryService(JdbcTemplate mJdbcTemplate) {
		this.jdbcTemplate = mJdbcTemplate;
		String createTable = "create table if not exist country(" + "id bigint not null auto_increment PRIMARY_KEY,"
				+ "country_code varchar(20)," + "country_name varchar(100)" + ")";
		this.jdbcTemplate.execute(createTable);

	}

	public void addCountry(Country country) {
		checkCountryInList(country);
		var insertQuery = "insert into country(country_code,country_name) values(?,?); ";
		int resultSet = jdbcTemplate.update(insertQuery, country.getCountryCode(), country.getCountryName());

		if (resultSet < 1) {
			throw new RuntimeException("Failed to create country");
		}
	}

	public void checkCountryInList(Country c) {
		var checkCountrykQuery = "select count(*) as count from country c where c.country_code = ?; ";
		var map = jdbcTemplate.queryForMap(checkCountrykQuery, c.getCountryCode());
		if (Integer.parseInt(map.get("count").toString()) > 0) {
			throw new ResourceAlreadyExistsException("Country with this country code already in DB.");
		}
	}
	
	public Country getCountryByCode(String countryCode) {
		var countryQuery = "select id,country_code,country_name from country where country_code = ? ;";
		var countryMap = jdbcTemplate.queryForMap(countryQuery,countryCode);
		var country = new Country();
		country.setId(Long.parseLong(countryMap.get("id").toString()));
		country.setCountryCode(countryMap.get("country_code").toString());
		country.setCountryName(countryMap.get("country_name").toString());
		return country;
	}

	public List<Country> getAllCountryList() {
		var listQuery = "select id,country_code,country_name from country";
		var countries = new ArrayList<Country>();
		jdbcTemplate.queryForList(listQuery).stream().forEach(countryMap -> {
			var country = new Country();
			country.setId(Long.parseLong(countryMap.get("id").toString()));
			country.setCountryCode(countryMap.get("country_code").toString());
			country.setCountryName(countryMap.get("country_name").toString());
			countries.add(country);
		});

		return countries;
	}

}
