package com.example.springmvc.jdbc.model;

import java.io.Serializable;

public class Country implements Serializable {
	private long id;
	private String countryName;
	private String countryCode;

	public Country() {
		super();
	}

	@Override
	public String toString() {
		return "Country [id=" + id + ", countryName=" + countryName + ", countryCode=" + countryCode + ", getId()="
				+ getId() + ", getCountryName()=" + getCountryName() + ", getCountryCode()=" + getCountryCode()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

	public Country(String countryName, String countryCode) {
		super();
		this.countryName = countryName;
		this.countryCode = countryCode;
	}

	public Country(int id, String countryName) {
		super();
		this.id = id;
		this.countryName = countryName;
	}

	public Country(int id) {
		super();
		this.id = id;
	}

	public Country(int id, String countryName, String countryCode) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.countryCode = countryCode;
	}

	public long getId() {
		return id;
	}

	public void setId(long l) {
		this.id = l;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
