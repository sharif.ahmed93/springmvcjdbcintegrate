package com.example.springmvc.jdbc.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.example.springmvc.jdbc"})
public class RootConfig {

}
